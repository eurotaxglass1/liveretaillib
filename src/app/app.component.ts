import { Component } from '@angular/core';
import { OwnVehicleParams } from 'liveRetail-lib/interfaces/own_vehicle';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'container-app';
	// Example of parameters that you need to pass to live-retail-lib.

	ownVehicleParameters: OwnVehicleParams = {
		// "ownVehicle": {
		// 	"askingPrice": 13959,
		// 	"isoLanguage": 'en',
		// 	"odometer": 9000,
		// 	"regDate": '2011-12-31T00:00:00.000Z',
		// 	"zipcode": "9020"

		// TO TEST 'DE' MARKET WITH CORRECT NATCODE AND DATA
		// ownVehicle: {
		// 	askingPrice: 10000,
		// 	isoCountry: 'DE',
		// 	isoLanguage: 'de',
		// 	vehicleType: '10',
		// 	odometer: 20000,
		// 	regDate: '2013-04-15T00:00:00.000Z',
		// 	regNo: 'IMC-248',
		// 	regionCode: 1,
		// 	natCode: 10417645,
		// 	zipcode: '63477'

		// TO TEST 'UK' MARKET WITH CORRECT NATCODE AND DATA
		// ownVehicle: {
		// 	askingPrice: 1000,
		// 	isoCountry: 'UK',
		// 	isoLanguage: 'en',
		// 	vehicleType: '10',

		// 	regDate: '2006-10-31T00:00:00.000Z',
		// 	natCode: 107825001,
		// 	zipcode: 'TW12'
		// },

		// TO TEST 'UK' MARKET WITH CORRECT NATCODE AND DATA but not enough to compare
		// ownVehicle: {
		//     askingPrice: 7420,
		//     isoCountry: 'GB',
		//     isoLanguage: 'en',
		//     vehicleType: '10',
		//     odometer: 37630,
		//     regDate: '2016-03-01T00:00:00.000Z',
		//     natCode: 224857001,
		//     zipcode: 'JE2 3XR'
		// },


		// TO TEST CH MKT
		// ownVehicle: {
		// 	askingPrice: 7900,
		// 	isoCountry: 'CH',
		// 	isoLanguage: 'de',
		// 	odometer: 128000,
		// 	regDate: '2004-11-30T00:00:00.000Z',
		// 	natCode: 102167998,
		// 	zipcode: '2540',
		// 	vehicleType: '10'
		// }


		// TO TEST PL MKT
		// ownVehicle: {
		// 	askingPrice: 7260,
		// 	isoCountry: 'PL',
		// 	isoLanguage: 'pl',
		// 	odometer: 50000,
		// 	regDate: '2008-07-16T00:00:00.000Z',
		// 	natCode: 70788,
		// 	zipcode: '04-453',
		// 	vehicleType: '10'
		// }

		// TO TEST MODE 1 (with options list)
		// ownVehicle: {
		// 	askingPrice: 15000,
		// 	isoCountry: 'DE',
		// 	isoLanguage: 'de',
		// 	odometer: 20000,
		// 	regDate: '2013-04-15T00:00:00.000Z',
		// 	natCode: 10417645,
		// 	zipcode: '63477',
		// 	vehicleType: '10',
		// 	optionList: [
		// 		{ optionPrice: 99, optionCodeType: 'ESACO', esacoCodes: [ '752303', '752301', '751629' ] },
		// 		{ optionPrice: 999, optionCodeType: 'ESACO', esacoCodes: [ '754915' ] }
		// 	]
		// }

		// TO TEST MODE 2 (empty options list)
		// ownVehicle: {
		// 	askingPrice: 15000,
		// 	isoCountry: 'DE',
		// 	isoLanguage: 'en',
		// 	odometer: 20000,
		// 	regDate: '2013-04-15T00:00:00.000Z',
		// 	natCode: 10417645,
		// 	zipcode: '63477',
		// 	vehicleType: '10',
		// 	optionList: []
		// }

		// TO TEST MODE 3 (uknown options)
		ownVehicle: {
			askingPrice: 29500,
			isoCountry: 'DE',
			isoLanguage: 'de',
			odometer: 109000,
			regDate: '2015-09-15T00:00:00.000Z',
			natCode: 10384451,
			zipcode: '30952',
			vehicleType: '10'
		},

		// TO TEST CONCRET CASES
		// ownVehicle: {
		// 	askingPrice: 30900,
		// 	isoCountry: 'AT',
		// 	isoLanguage: 'de',
		// 	odometer: 20654,
		// 	regDate: '2014-06-29T00:00:00.000Z',
		// 	natCode: 177743,
		// 	zipcode: '1230',
		// 	vehicleType: '10',
		// 	optionList: [
		// 		{ optionPrice: 1255, avecId: '10033' },
		// 		{ optionPrice: 520, avecId: '10005' },
		// 		{ optionPrice: 920, avecId: '10002' },
		// 		{ optionPrice: 3095, avecId: '10000' }
		// 	]
		// }

		// TO TEST ASKING PRICE ERROR MESSAGE 
		// ownVehicle: {
		// 	askingPrice: 0,
		// 	isoCountry: 'CH',
		// 	isoLanguage: 'fr',
		// 	odometer: 9000,
		// 	regDate: '2011-12-31T00:00:00.000Z',
		// 	natCode: 177416,
		// 	zipcode: '9020',
		// 	vehicleType: '10'
		// },
		// ownVehicle: {
		// 	askingPrice: 13959,
		// 	isoCountry: 'AT',
		// 	isoLanguage: 'de',
		// 	odometer: 9000,
		// 	regDate: '2011-12-31T00:00:00.000Z',
		// 	natCode: 177416,
		// 	zipcode: '9020',
		// 	vehicleType: '10'
		// }

		// ownVehicle: {
		// 	askingPrice: 20740,
		// 	isoLanguage: 'fi',
		// 	isoCountry: 'FI',
		// 	odometer: 210000,
		// 	regDate: '2008-12-22T00:00:00.000Z',
		// 	zipcode: '33200',
		// 	optionValue: 2300,
		// 	natCode: 27134,
		// 	vehicleType: '10'
		// }

		//TO TEST OPTION CASES IN SAME LIST
		// ownVehicle: {
		// 	askingPrice: 29900,
		// 	isoLanguage: 'de',
		// 	isoCountry: 'CH',
		// 	odometer: 8400,
		// 	regDate: '2019-10-01T00:00:00.000Z',
		// 	zipcode: '6828',
		// 	natCode: 102216481,
		// 	vehicleType: '10'
		// }

		// TO TEST FI MKT
		// ownVehicle: {
		// 	askingPrice: 12650,
		// 	isoCountry: 'FI',
		// 	isoLanguage: 'fi',
		// 	odometer: 25000,
		// 	regDate: '2015-04-10T00:00:00.000Z',
		// 	natCode: 80217,
		// 	zipcode: '02240',
		// 	vehicleType: '10'
		// }

		// ownVehicle: {
		// 	askingPrice: 12650,
		// 	isoCountry: 'FI',
		// 	isoLanguage: 'fi',
		// 	odometer: 25000,
		// 	regDate: '2015-04-10T00:00:00.000Z',
		// 	natCode: 80217,
		// 	zipcode: '02240',
		// 	vehicleType: '10',
		// 	optionValue: 2300
		// }

		//----------------------------------------------------

		// FRONT ERROR PAGES: 

		//TO TEST OWNVEHICLE CASE ERROR: No enough data to compare
		// ownVehicle: {
		// 	askingPrice: 7900,
		// 	isoCountry: 'CH',
		// 	isoLanguage: 'de',
		// 	odometer: 128000,
		// 	regDate: '2021-11-30T00:00:00.000Z',
		// 	natCode: 102167998,
		// 	zipcode: '2540',
		// 	vehicleType: '10'
		// }

		//TO TEST ERROR CASE: MISSING MANDATORY PARAMS () (isoCountry and Natcode i.e)
		// ownVehicle: {
		// 	askingPrice: 7900,
		// 	// IsoCountry
		// 	isoLanguage: 'de',
		// 	odometer: 128000,
		// 	regDate: '2021-11-30T00:00:00.000Z',
		// 	// Natcode
		// 	zipcode: '2540',
		// 	vehicleType: '10'
		// }

	};

	constructor() { }
}
