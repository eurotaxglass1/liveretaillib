import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { liveRetailApiURL } from "./env-config";

@Injectable({ providedIn: 'root'})
export class InterceptorService implements HttpInterceptor {
    
    constructor(){

    }
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (req.url.search('^\/live-retail-api') === 0){
            var newUrl = liveRetailApiURL + req.url.replace('/live-retail-api', '')
            const authReq = req.clone({
                url: newUrl,
                headers: new HttpHeaders({
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-avh-realm': 'dc',
                    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWJqZWN0SWQiOjk1NzgsInByaW5jaXBhbElkIjo2Mjc0LCJpYXQiOjE3MTIxMzQ2MDQsImV4cCI6MTcxMjIyMTAwNH0.o1pNjKFmSNtZHnpauAEd5lrcjvqUTIsTWYYCFckldnk'
                })
            })
            
            // console.log('Intercepted HTTP call', authReq);
            return next.handle( authReq );
        }
        else {
            console.log("Not proccessing this request")
        }
    }
        
}