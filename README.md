# ContainerApp

This App is a simple container with images to test the "liveRetail-lib" inside it. (Blue rectangle)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Publishing

1. Update version in `projects\liveRetail-lib\package.json`
2. (if publishing for the first time) Execute `npm adduser --registry http://artifactory.autovista.eu/repository/npm-autovista/` (trailing slash is required)
3. Execute `npm run pack`
4. Execute `npm run publish`


