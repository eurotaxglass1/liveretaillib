import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { LiveRetailLibService } from '../../liveRetail-lib.service';


@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit, OnChanges {
	@Input()
	  advertsObject: any;
	  
	summaryProps: any;
	mktProps: any;
	tooltipLoadingFlag: boolean;
	currencySymbolBefore: boolean = false;

	constructor(private dataService: LiveRetailLibService) {
		this.tooltipLoadingFlag = true;
	}
	ngOnInit(){
		this.summaryProps = this.dataService.getLanguageProperties().summary;
		this.mktProps = this.dataService.getMarketConfig().config;
		if (this.mktProps.currencySymbol == '£'){
			this.currencySymbolBefore = true;
		}else {
			this.currencySymbolBefore = false;
		}
	}

	ngOnChanges(): void {	
		if(this.advertsObject) this.tooltipLoadingFlag = false;
		else this.tooltipLoadingFlag = true;
	}

}
