import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, UntypedFormBuilder } from '@angular/forms';
import { OwnVehicleParams } from '../../interfaces/own_vehicle';
import { Observable } from 'rxjs';
import { LiveRetailLibService } from '../../liveRetail-lib.service';
import { FiltersOptionsService } from '../../services/filters-options.service';
import {
  style,
  animate,
  transition,
  trigger,
  state,
  AUTO_STYLE,
  group,
} from '@angular/animations';

const animationTime = 350;

@Component({
	selector: 'app-filters',
	templateUrl: './filters.component.html',
	styleUrls: [ './filters.component.scss' ],
	animations: [
		trigger('collapse', [
			state('false', style({ height: AUTO_STYLE, visibility: AUTO_STYLE, opacity: 1, overflow: 'initial' })),
			state('true', style({ height: '0', visibility: 'hidden', opacity: 0, overflow: 'hidden' })),
			transition('false => true',  group([
				animate(animationTime + 'ms ease-in', style({ height: '0', visibility: 'hidden', opacity: 0})),
				// overflow hidden just after animation starts
				animate(animationTime + 'ms steps(1, start)', style({ overflow: 'hidden' }))
			])),

			transition('true => false',  group([
				animate(animationTime + 'ms ease-out', style({ height: AUTO_STYLE, visibility: AUTO_STYLE, opacity: 1 })),
				// overflow initial just before animation ends
				animate(animationTime + 'ms steps(1, end)', style({ overflow: 'initial' }))
			])),
		  ])
	  ]
})
export class FiltersComponent implements OnInit {
	@Output() 
	advertsUpdate = new EventEmitter<any>();
	@Input()
	ownVehicle: OwnVehicleParams;

	filtersProps: any;
	filtersLink: boolean;
	filtersLinkTxt: string;
	searchCriteria$: Observable<any>;
	searchCriteria: any;
	yearsInterval: Array<any> = [];
	currentYear: number;
	fuelDisabled: boolean = false;
	bodyTypeDisabled: boolean = false;
	gearBoxDisabled: boolean = false;
	driveTypeDisabled: boolean = false;
	market: string;
	distanceMappedList: Array<any> = [];
	defaultDistance: any;
	@ViewChild('collapseFiltersDiv') collapseFiltersDiv: ElementRef;
	filtersForm: UntypedFormGroup;
	mileageList: Array<any> = [];

	limitSelection = false;
	selectedItems: Array<any> = [];
	dropdownSettings: any = {};
	dropdownSettingsSingle: any = {};
	showSellerFilters: boolean = true;
	collapseFilters = true;


	constructor(
		private dataService: LiveRetailLibService,
		private filterOptionsService: FiltersOptionsService,
		private formBuilder: UntypedFormBuilder
	) {
		this.currentYear = new Date().getFullYear();
		this.filtersForm = this.formBuilder.group({
			keywords: new UntypedFormControl(),
			mileageFrom: new UntypedFormControl(''),
			mileageTo: new UntypedFormControl(''),
			yearFrom: new UntypedFormControl(''),
			yearTo: new UntypedFormControl(''),
			engineFrom: new UntypedFormControl(''),
			engineTo: new UntypedFormControl(''),
			fuelType: new UntypedFormControl(''),
			bodyType: new UntypedFormControl(''),
			transmission: new UntypedFormControl(''),
			drivetrain: new UntypedFormControl(''),
			powerFrom: new UntypedFormControl(''),
			powerTo: new UntypedFormControl(''),
			distance: new UntypedFormControl(''),
			onSaleCheck: new UntypedFormControl(''),
			soldCheck: new UntypedFormControl(''),
			dealerCheck: new UntypedFormControl(''),
			privateCheck: new UntypedFormControl(''),
		});
	}

	ngOnInit() {
		this.filtersProps = this.dataService.getLanguageProperties().filters;
		this.filtersLink = true;
		this.filtersLinkTxt = this.filtersProps.showFilters;

		this.getSearchCriteria();
		this.getYearsCounter();
		this.getMileageList();
		this.setFiltersValues();

		this.dropdownSettings = {
			singleSelection: false,
			idField: 'code',
			textField: 'description',
			selectAllText: this.filtersProps.nullValid,
			unSelectAllText: this.filtersProps.nullValid,
			itemsShowLimit: 1,
			allowSearchFilter: false
		};

		this.dropdownSettingsSingle = {
			singleSelection: true,
			idField: 'code',
			textField: 'description',
			selectAllText: this.filtersProps.nullValid,
			unSelectAllText: this.filtersProps.nullValid,
			itemsShowLimit: 1,
			allowSearchFilter: false
		}
		
	}

	handleLimitSelection() {
		if (this.limitSelection) {
			this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: 2 });
		} else {
			this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: null });
		}
	}

	getSearchCriteria() {
		this.searchCriteria$ = this.filterOptionsService.resolveFiltersOptions(
			this.ownVehicle.ownVehicle.isoCountry, 
			this.ownVehicle.ownVehicle.isoLanguage, 
			this.ownVehicle.ownVehicle.natCode);
		this.searchCriteria$.subscribe((a) => {
			this.searchCriteria = a;
			// console.log("search criteria response: ", this.searchCriteria)
			for (let i = 0; i < this.searchCriteria.distanceList.length; i++) {
				if (this.searchCriteria.distanceList[i].isDefault === true) {
					if(this.searchCriteria.distanceList[i].description == 'ALL'){
						this.defaultDistance = this.searchCriteria.distanceList[i].code;
						// this.defaultDistance = this.filtersProps.wholeCountry;
					} else {
						this.defaultDistance = this.searchCriteria.distanceList[i].description;
					}
				}
			};
			// console.log(this.defaultDistance)
			this.filtersForm.controls['onSaleCheck'].setValue(this.searchCriteria?.defaultInclude.vehiclesForSale);
			this.filtersForm.controls['soldCheck'].setValue(this.searchCriteria?.defaultInclude.vehiclesAlreadySold);
			this.filtersForm.controls['dealerCheck'].setValue(this.searchCriteria?.defaultInclude.dealerSales);
			this.filtersForm.controls['privateCheck'].setValue(this.searchCriteria?.defaultInclude.privateSales);
			this.filtersForm.controls['distance'].setValue(this.defaultDistance);

			this.bodyTypeDisabled = a.displayOptions.disableBodyFilter;
			this.driveTypeDisabled = a.displayOptions.disableDrivetrainFilter;
			this.fuelDisabled = a.displayOptions.disableFuelFilter;
			this.gearBoxDisabled = a.displayOptions.disableTransmissionFilter;

			if(a.displayOptions.showSellerFilter == false) {
				this.showSellerFilters = false;
				this.filtersForm.controls['dealerCheck'].setValue(undefined);
				this.filtersForm.controls['privateCheck'].setValue(undefined);
			}
		});
		
		return this.distanceMappedList;
	}

	getYearsCounter() {
		if (this.currentYear) {
			for (let i = 1986; i <= this.currentYear; i++) {
				this.yearsInterval.push(i);
			}
		}
		return this.yearsInterval.reverse();
	}

	getMileageList(){
		this.mileageList[0] = 25000;
		for (let i = 1; i < 12; i++){
			this.mileageList[i] = this.mileageList[i-1] + 25000;
		}
		return this.mileageList
	}

	setFiltersValues() {
		this.filtersForm.controls['keywords'].setValue('');
		this.filtersForm.controls['mileageFrom'].setValue(undefined);
		this.filtersForm.controls['mileageTo'].setValue(undefined);
		this.filtersForm.controls['yearFrom'].setValue(undefined);
		this.filtersForm.controls['yearTo'].setValue(this.currentYear);
		this.filtersForm.controls['engineFrom'].setValue(undefined);
		this.filtersForm.controls['engineTo'].setValue(undefined);
		this.filtersForm.controls['fuelType'].setValue(null);
		this.filtersForm.controls['bodyType'].setValue(null);
		this.filtersForm.controls['transmission'].setValue(null);
		this.filtersForm.controls['drivetrain'].setValue(null);
		this.filtersForm.controls['powerFrom'].setValue(undefined);
		this.filtersForm.controls['powerTo'].setValue(undefined);
	}

	toggleFiltersLink() {
		this.collapseFilters = !this.collapseFilters;
		if (
			this.filtersLink == false &&
			this.collapseFiltersDiv.nativeElement.classList.contains('collapsing') == false
		) {
			this.filtersLinkTxt = this.filtersProps.showFilters;
			this.filtersLink = true;
		} else if (
			this.filtersLink == true &&
			this.collapseFiltersDiv.nativeElement.classList.contains('collapsing') == false
		) {
			this.filtersLinkTxt = this.filtersProps.hideFilters;
			this.filtersLink = false;
		}
	}

	postData() {
		let mileage = {from: null, to: null};
		let year = {from:null, to: this.currentYear.toString()};
		let enginePower = {from:null, to: null};
		let engineSize = {from:null, to: null};
		//
		let fuelTypeList = [];
		let bodyTypeList = [];
		let transmissionList = [];
		let driveList = [];

		if (this.filtersForm.value.mileageFrom == 'null' || !this.filtersForm.value.mileageFrom) mileage.from = null; 
			else mileage.from = this.filtersForm.value.mileageFrom[0];
		if (this.filtersForm.value.mileageTo == 'null' || !this.filtersForm.value.mileageTo) mileage.to = null;
			else mileage.to = this.filtersForm.value.mileageTo[0];
		if (this.filtersForm.value.yearFrom == 'null' || !this.filtersForm.value.yearFrom) year.from = null;
			else year.from = this.filtersForm.value.yearFrom[0];
		if (this.filtersForm.value.yearTo == 'null' || !this.filtersForm.value.yearTo) year.to = this.currentYear.toString();
			else year.to = this.filtersForm.value.yearTo[0];
		if (this.filtersForm.value.powerFrom == 'null' || !this.filtersForm.value.powerFrom || !this.filtersForm.value.powerFrom[0]) enginePower.from = null;
			else enginePower.from = this.filtersForm.value.powerFrom[0].code;
	 	if (this.filtersForm.value.powerTo == 'null' || !this.filtersForm.value.powerTo || !this.filtersForm.value.powerTo[0]) enginePower.to = null;
			else enginePower.to = this.filtersForm.value.powerTo[0].code;
		if (this.filtersForm.value.engineFrom == 'null' || !this.filtersForm.value.engineFrom || !this.filtersForm.value.engineFrom[0]) engineSize.from = null;
			else engineSize.from = this.filtersForm.value.engineFrom[0].code;
		if (this.filtersForm.value.engineTo == 'null' || !this.filtersForm.value.engineTo || !this.filtersForm.value.engineTo[0]) engineSize.to = null;
			else engineSize.to = this.filtersForm.value.engineTo[0].code;
		//
		
		if (this.filtersForm.value.fuelType == null || this.filtersForm.value.fuelType == 'null') fuelTypeList = null;
			else {
				for (let i = 0;i  < this.filtersForm.value.fuelType.length; i++){
					fuelTypeList.push(this.filtersForm.value.fuelType[i].code); 
				}
			} 
		if (this.filtersForm.value.bodyType == null || this.filtersForm.value.bodyType == 'null') bodyTypeList = null;
			else {
				for (let i = 0;i  < this.filtersForm.value.bodyType.length; i++){
					bodyTypeList.push(this.filtersForm.value.bodyType[i].code); 
				}
			} 
		if (this.filtersForm.value.transmission == null || this.filtersForm.value.transmission == 'null') transmissionList = null;
			else {
				for (let i = 0;i  < this.filtersForm.value.transmission.length; i++){
					transmissionList.push(this.filtersForm.value.transmission[i].code); 
				}
			} 
		if (this.filtersForm.value.drivetrain == null || this.filtersForm.value.drivetrain == 'null') driveList = null;
			else {
				for (let i = 0;i  < this.filtersForm.value.drivetrain.length; i++){
					driveList.push(this.filtersForm.value.drivetrain[i].code); 
				}
			}
		
		let searchParameters = {
			"searchCriteria": {
				"include": {
					"vehiclesForSale": this.filtersForm.value.onSaleCheck, // Need to extract disabled checkbxes from searchCriteria and then send it like null
					"vehiclesAlreadySold": this.filtersForm.value.soldCheck,
					"privateSales": this.filtersForm.value.privateCheck,
					"dealerSales": this.filtersForm.value.dealerCheck
				},
				mileage,
				year,
				enginePower,
				engineSize,
				fuelTypeList,
				bodyTypeList,
				transmissionList,
				driveList,
				"distance": this.filtersForm.value.distance,
				"keyword": this.filtersForm.value.keywords
			}
		};
		// console.log(searchParameters);
		this.advertsUpdate.emit(searchParameters);
	}
}