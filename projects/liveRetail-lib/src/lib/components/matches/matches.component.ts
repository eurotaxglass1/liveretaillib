import { Component, Input, OnInit } from '@angular/core';
import { Live } from '@ng-bootstrap/ng-bootstrap/util/accessibility/live';
import { LiveRetailLibService } from '../../liveRetail-lib.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss']
})
export class MatchesComponent implements OnInit {

  @Input()
  advertsObject: any;
  
  matchesProps: any;
  lngConfig: String;

  constructor(private dataService: LiveRetailLibService) {
	}

	ngOnInit() {
    this.lngConfig = this.dataService.getLanguageConfig();
    this.matchesProps = this.dataService.getLanguageProperties().matches;
  }

}
