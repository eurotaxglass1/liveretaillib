import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';

import {
	ChartComponent,
	ApexAxisChartSeries,
	ApexChart,
	ApexYAxis,
	ApexXAxis,
	ApexOptions,
	ApexAnnotations,
	ApexGrid,
	ApexTooltip,
	ApexLegend,
	ApexMarkers
} from 'ng-apexcharts';

import { LiveRetailLibService } from '../../liveRetail-lib.service';

export type ChartOptions = {
	series: ApexAxisChartSeries;
	chart: ApexChart;
	xaxis: ApexXAxis;
	yaxis: ApexYAxis;
	colors: ApexOptions;
	annotations: ApexAnnotations;
	grid: ApexGrid;
	tooltip: ApexTooltip;
	legend: ApexLegend;
	markers: ApexMarkers;
};

@Component({
	selector: 'app-scatter-graph',
	templateUrl: './scatter-graph.component.html',
	styleUrls: [ './scatter-graph.component.scss' ]
})
export class ScatterGraphComponent implements OnInit, OnChanges {
	@ViewChild('chart') 
	chart: ChartComponent;
	public chartOptions: any;
	@Input() 
	listOfVehicles: Array<any>;
	tooltipLoadingFlag: boolean;
	scatterLoading: boolean;
	exactMatchValues = [];
	bestMatchValues = [];
	closeMatchValues = [];
	similarMatchValues = [];
	ownVehicleValues = [];
	matchesProps: any;
	reportProps: any;
	resultsProps: any;
	mktProps: any;
	exactMatchesVehicles: any = [];
	bestMatchesVehicles: any = [];
	closeMatchesVehicles: any = [];
	similarMatchesVehicles: any = [];
	currencySymbolBefore: boolean = false;
	ownVehiclePriceDifferenceCurrency: number;

	constructor(private dataService: LiveRetailLibService) {
		this.tooltipLoadingFlag = true;
		this.scatterLoading = true;
	}

	ngOnInit(){
		this.matchesProps = this.dataService.getLanguageProperties().matches;
		this.reportProps = this.dataService.getLanguageProperties().report;
		this.resultsProps = this.dataService.getLanguageProperties().results;
		this.mktProps = this.dataService.getMarketConfig().config;
		if (this.mktProps.currencySymbol == '£'){
			this.currencySymbolBefore = true;
		}else{
			this.currencySymbolBefore = false;
		}
	}

	ngOnChanges() {
		this.getGraphData();
	}

	openGraph(){
		this.scatterLoading = true;
		setTimeout(() => this.scatterLoading = false, 1000)
	}

	getGraphData() {
		this.tooltipLoadingFlag = true;
		// console.log(this.listOfVehicles)

		if (this.listOfVehicles && this.listOfVehicles.length > 1 && this.listOfVehicles[0]) {
			for (let i = 0; i < this.listOfVehicles.length; i++) {
				if (this.listOfVehicles[i].similarityCategory === 0 && this.listOfVehicles[i].isOwnVehicle === false) {
					this.exactMatchValues.push([
						this.listOfVehicles[i].regionalAskingPrice,
						this.listOfVehicles[i].askingPrice
					]);
					this.exactMatchesVehicles.push(this.listOfVehicles[i]);
				}
				//set own vehicle.
				if (this.listOfVehicles[i].isOwnVehicle === true){
					this.ownVehiclePriceDifferenceCurrency = this.listOfVehicles[i].priceDifferenceCurrency;
					this.ownVehicleValues.push([
						this.listOfVehicles[i].regionalAskingPrice,
						this.listOfVehicles[i].askingPrice
					]);
				}	
			}

			for (let i = 0; i < this.listOfVehicles.length; i++) {
				if (this.listOfVehicles[i].similarityCategory === 1) {
					this.bestMatchValues.push([
						this.listOfVehicles[i].regionalAskingPrice,
						this.listOfVehicles[i].askingPrice
					]);
					this.bestMatchesVehicles.push(this.listOfVehicles[i]);
				}
			}

			for (let i = 0; i < this.listOfVehicles.length; i++) {
				if (this.listOfVehicles[i].similarityCategory === 2) {
					this.closeMatchValues.push([
						this.listOfVehicles[i].regionalAskingPrice,
						this.listOfVehicles[i].askingPrice
					]);
					this.closeMatchesVehicles.push(this.listOfVehicles[i]);
				}
			}

			for (let i = 0; i < this.listOfVehicles.length; i++) {
				if (this.listOfVehicles[i].similarityCategory === 3) {
					this.similarMatchValues.push([
						this.listOfVehicles[i].regionalAskingPrice,
						this.listOfVehicles[i].askingPrice
					]);
					this.similarMatchesVehicles.push(this.listOfVehicles[i]);
				}
			}
			this.tooltipLoadingFlag = false;
			this.graphConfig();
		} else {
			this.exactMatchValues = [];
			this.bestMatchValues = [];
			this.closeMatchValues = [];
			this.similarMatchValues = [];
			this.ownVehicleValues = [];
			this.bestMatchesVehicles = [];
			this.exactMatchesVehicles = [];
			this.closeMatchesVehicles = [];
			this.similarMatchesVehicles = [];
			this.tooltipLoadingFlag = true;
		}
	}

	graphConfig() {
		var exactVehicles = this.exactMatchesVehicles;
		var bestVehicles = this.bestMatchesVehicles;
		var closeVehicles = this.closeMatchesVehicles;
		var similarVehicles = this.similarMatchesVehicles;
		var ownVehicle = this.ownVehicleValues;

		var exactKey = this.matchesProps.exact.toUpperCase();
		var bestKey = this.matchesProps.best.toUpperCase();
		var closeKey = this.matchesProps.close.toUpperCase();
		var similarKey = this.matchesProps.similar.toUpperCase();


		function findAdExact(val) {
			if(exactVehicles[val]?.advertisementUrl && exactVehicles[val]?.advertisementUrl != '#') {window.open(exactVehicles[val].advertisementUrl);}
		}

		function findAdBest(val) {
			if(bestVehicles[val]?.advertisementUrl && bestVehicles[val]?.advertisementUrl != '#') {window.open(bestVehicles[val].advertisementUrl);}
		}

		function findAdClose(val) {
			if(closeVehicles[val]?.advertisementUrl && closeVehicles[val]?.advertisementUrl != '#') {window.open(closeVehicles[val].advertisementUrl);}
		}

		function findAdSimilar(val) {
			if(similarVehicles[val]?.advertisementUrl && similarVehicles[val]?.advertisementUrl != '#') {window.open(similarVehicles[val].advertisementUrl);}
		}


		var exactMatchSerie = {
			name: this.matchesProps.exact.toUpperCase(),
			data: this.exactMatchValues
		};
		var bestMatchSerie = {
			name: this.matchesProps.best.toUpperCase(),
			data: this.bestMatchValues
		};
		var closeMatchSerie = {
			name: this.matchesProps.close.toUpperCase(),
			data: this.closeMatchValues
		};
		var similarMatchSerie = {
			name: this.matchesProps.similar.toUpperCase(),
			data: this.similarMatchValues
		};
		var ownVehicleSerie = {
			name: this.resultsProps.ownVehicle.toUpperCase(),
			data: this.ownVehicleValues
		};


		var seriesObject;
		var colorsObject;
		var currencySymbol = this.mktProps.currencySymbol;
		var thousandSymbol = this.mktProps.valueSeparator;
		var graphTextAvg = this.reportProps.graph.avg.toUpperCase();
		var graphTextLiveRetailPrice = this.reportProps.graph.liveRetailPrice.toUpperCase();
		var graphTextAsking = this.resultsProps.askPrice.toUpperCase();
		var graphTextLrp = this.reportProps.graph.lrp.toUpperCase();

		var overpricedTxt = this.resultsProps.tooltips.priceDiff.overpriced;
		var underpricedTxt = this.resultsProps.tooltips.priceDiff.underpriced;
		var priceDiff: any;

		var ownVhPriceDiff = this.ownVehiclePriceDifferenceCurrency;
		// console.log(ownVhPriceDiff)

		function setPriceDiffLabel(value){
			switch (value.seriesIndex) {
				case 0:
					// console.log(exactVehicles[value.dataPointIndex].priceDifferenceCurrency);
					if(exactVehicles[value.dataPointIndex].priceDifferenceCurrency > 0){
						if(currencySymbolBefore){
							priceDiff = `<span style="color: #DE4A39;">[ ${overpricedTxt}: ${currencySymbol} ${exactVehicles[value.dataPointIndex].priceDifferenceCurrency} ]</span>`;
						}else {
							priceDiff = `<span style="color: #DE4A39;">[ ${overpricedTxt}: ${exactVehicles[value.dataPointIndex].priceDifferenceCurrency} ${currencySymbol} ]</span>`;
						}
					}else if (exactVehicles[value.dataPointIndex].priceDifferenceCurrency < 0){
						if(currencySymbolBefore){
							priceDiff = `<span style="color: #008FC8;">[ ${underpricedTxt}: ${currencySymbol} ${exactVehicles[value.dataPointIndex].priceDifferenceCurrency} ]</span>`;
						}else{
							priceDiff = `<span style="color: #008FC8;">[ ${underpricedTxt}: ${exactVehicles[value.dataPointIndex].priceDifferenceCurrency} ${currencySymbol} ]</span>`;
						}
					} else {
						priceDiff = ''
					}
					return priceDiff ;
				case 1:
					// console.log(bestVehicles[value.dataPointIndex].priceDifferenceCurrency);
					if(bestVehicles[value.dataPointIndex].priceDifferenceCurrency > 0){
						if(currencySymbolBefore){
							priceDiff = `<span style="color: #DE4A39;">[ ${overpricedTxt}: ${currencySymbol} ${bestVehicles[value.dataPointIndex].priceDifferenceCurrency} ]</span>`;

						}else{
							priceDiff = `<span style="color: #DE4A39;">[ ${overpricedTxt}: ${bestVehicles[value.dataPointIndex].priceDifferenceCurrency} ${currencySymbol} ]</span>`;
						}
					}else if (bestVehicles[value.dataPointIndex].priceDifferenceCurrency < 0){
						if(currencySymbolBefore){
							priceDiff = `<span style="color: #008FC8;">[ ${underpricedTxt}: ${currencySymbol} ${bestVehicles[value.dataPointIndex].priceDifferenceCurrency} ]</span>`;

						}else{
							priceDiff = `<span style="color: #008FC8;">[ ${underpricedTxt}: ${bestVehicles[value.dataPointIndex].priceDifferenceCurrency} ${currencySymbol} ]</span>`;
						}
					} else {
						priceDiff = ''
					}
					return priceDiff;
				case 2:
					// console.log(closeVehicles[value.dataPointIndex].priceDifferenceCurrency);
					if(closeVehicles[value.dataPointIndex].priceDifferenceCurrency > 0){
						if(currencySymbolBefore){
							priceDiff = `<span style="color: #DE4A39;">[ ${overpricedTxt}: ${currencySymbol} ${closeVehicles[value.dataPointIndex].priceDifferenceCurrency} ]</span>`;
						}else{
							priceDiff = `<span style="color: #DE4A39;">[ ${overpricedTxt}: ${closeVehicles[value.dataPointIndex].priceDifferenceCurrency} ${currencySymbol} ]</span>`;
						}
					}else if (closeVehicles[value.dataPointIndex].priceDifferenceCurrency < 0){
						if(currencySymbolBefore){
							priceDiff = `<span style="color: #008FC8;">[ ${underpricedTxt}: ${currencySymbol} ${closeVehicles[value.dataPointIndex].priceDifferenceCurrency} ]</span>`;
						}else{
							priceDiff = `<span style="color: #008FC8;">[ ${underpricedTxt}: ${closeVehicles[value.dataPointIndex].priceDifferenceCurrency} ${currencySymbol} ]</span>`;
						}
					} else {
						priceDiff = ''
					}
					return priceDiff;
				case 3:
					// console.log(similarVehicles[value.dataPointIndex].priceDifferenceCurrency);
					if(similarVehicles[value.dataPointIndex].priceDifferenceCurrency > 0){
						if(currencySymbolBefore){
							priceDiff = `<span style="color: #DE4A39;">[ ${overpricedTxt}: ${currencySymbol} ${similarVehicles[value.dataPointIndex].priceDifferenceCurrency} ]</span>`;
						}else{
							priceDiff = `<span style="color: #DE4A39;">[ ${overpricedTxt}: ${similarVehicles[value.dataPointIndex].priceDifferenceCurrency} ${currencySymbol} ]</span>`;
						}
					}else if (similarVehicles[value.dataPointIndex].priceDifferenceCurrency < 0){
						if(currencySymbolBefore){
							priceDiff = `<span style="color: #008FC8;">[ ${underpricedTxt}: ${currencySymbol} ${similarVehicles[value.dataPointIndex].priceDifferenceCurrency} ]</span>`;
						}else{
							priceDiff = `<span style="color: #008FC8;">[ ${underpricedTxt}: ${similarVehicles[value.dataPointIndex].priceDifferenceCurrency} ${currencySymbol} ]</span>`;
						}
					} else {
						priceDiff = ''
					}
					return priceDiff;
				case 4:
					if(ownVhPriceDiff > 0){
						if(currencySymbolBefore){
							priceDiff = `<span style="color: #DE4A39;">[ ${overpricedTxt}: ${currencySymbol} ${ownVhPriceDiff} ]</span>`;
						}else{
							priceDiff = `<span style="color: #DE4A39;">[ ${overpricedTxt}: ${ownVhPriceDiff} ${currencySymbol} ]</span>`;
						}
					}else if (ownVhPriceDiff < 0){
						if(currencySymbolBefore){
							priceDiff = `<span style="color: #008FC8;">[ ${underpricedTxt}: ${currencySymbol} ${ownVhPriceDiff} ]</span>`;
						}else{
							priceDiff = `<span style="color: #008FC8;">[ ${underpricedTxt}: ${ownVhPriceDiff} ${currencySymbol} ]</span>`;
						}
					} else {
						priceDiff = ''
					}
					return priceDiff;
			}
		}
		

		var currencySymbolBefore = false;
		if(currencySymbol == '£') currencySymbolBefore = true;
		
		seriesObject = [exactMatchSerie, bestMatchSerie, closeMatchSerie, similarMatchSerie, ownVehicleSerie]
		colorsObject = ['#00A76E', '#00A8BE', '#E1BE2B', '#DE4A39', '#111' ];		

		this.chartOptions = {
			annotations: {},
			tooltip: {
				shared: false,
				intersect: true,
				x: {
					style: {
						color: '#A1A9AC',
						fontSize: '11px'
					},
					formatter: (value) => {
						if (currencySymbolBefore) {
							return `${graphTextLrp} ${currencySymbol} ${numberWithCommas(value)}`;
						}else {
							return `${graphTextLrp} ${numberWithCommas(value)} ${currencySymbol}`;
						}
						function numberWithCommas(x) {
							return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandSymbol);
						}
					}
				},
				y: {
					style: {
						color: '#A1A9AC',
						fontSize: '11px'
					},
					formatter: (value) => {
						if (currencySymbolBefore) {
							return `${graphTextAsking} ${currencySymbol} ${numberWithCommas(value)}  ${priceDiff}`;
						}else {
							return `${graphTextAsking} ${numberWithCommas(value)} ${currencySymbol}  ${priceDiff}`;
						}
						function numberWithCommas(x) {
							return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandSymbol);
						}
					}
				}
			},
			series: seriesObject,
			colors: colorsObject,
			legend: {
				show: true,
				showForSingleSeries: true,
				showForNullSeries: true,
				showForZeroSeries: true,
				position: 'bottom',
			},
			chart: {
				animations: {
					enabled: false
				},
				height: 350,
				width: 600,
				type: 'scatter',
				zoom: {
					enabled: false
				},
				events: {
					dataPointSelection: function(event, chartContext, opts) {
						// console.log(opts.w.config.series[opts.seriesIndex].name);
						switch (opts.w.config.series[opts.seriesIndex].name) {
							case exactKey:
								findAdExact(opts.selectedDataPoints[0][0]);
								break;
							case bestKey:
								findAdBest(opts.selectedDataPoints[1][0]);
								break;
							case closeKey:
								findAdClose(opts.selectedDataPoints[2][0]);
								break;
							case similarKey:
								findAdSimilar(opts.selectedDataPoints[3][0]);
								break;
						}
					},
					dataPointMouseEnter: function(event, chartContext, opts){
						// console.log(opts.w.config.series[opts.seriesIndex].name);
						setPriceDiffLabel(opts)
					}
				}
			},
			xaxis: {
				tooltip: {
					enabled: false
				},
				tickAmount: 10,
				labels: {
					style: {
						color: '#A1A9AC',
						fontSize: '11px'
					},
					formatter: function numberWithCommas(x) {
						if(currencySymbolBefore){
							return currencySymbol + ' ' + Math.round(x).toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandSymbol);
						}
						else{
							return Math.round(x).toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandSymbol) + ' ' + currencySymbol;
						}
					}
				},
				title: {
					text: graphTextLiveRetailPrice
				}
			},
			yaxis: {
				tooltip: {
					enabled: false
				},
				tickAmount: 10,
				labels: {
					style: {
						color: '#A1A9AC',
						fontSize: '11px'
					},
					formatter: function numberWithCommas(x) {
						if(currencySymbolBefore){
							return currencySymbol + ' ' + Math.round(x).toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandSymbol);
						}
						else{
							return Math.round(x).toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandSymbol) + ' ' + currencySymbol;
						}
					}
				},
				title: {
					text: graphTextAsking
				}
			},
			grid: {
				show: true,
				borderColor: '#90A4AE',
				strokeDashArray: 0,
				position: 'back',
				xaxis: {
					lines: {
						show: true
					}
				},
				yaxis: {
					lines: {
						show: true
					}
				}
			},
			markers:{
				size: 7,
				shape: "square",
				radius: 1,
				hover: {
					size: 10
				  }
			}
		};
	}
}
