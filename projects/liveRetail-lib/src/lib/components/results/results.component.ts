import { AfterViewChecked, Component, Directive, EventEmitter, Input, OnDestroy, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { LiveRetailLibService } from '../../liveRetail-lib.service';
import { Vehicle } from '../../interfaces/vehicle';
import { Observable, Subscription } from 'rxjs';
import { VehicleDescriptionService } from '../../services/vehicle-description.service';
import { PriceHistoryService } from '../../services/price-history.service';
import { OwnVehicleParams } from '../../interfaces/own_vehicle';

//Internal sortable Directive
export type SortColumn = keyof Vehicle | '';
export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };
const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
export interface SortEvent {
	column: SortColumn;
	direction: SortDirection;
}
@Directive({
	selector: 'th[sortable]',
	host: {
		'[class.asc]': 'direction === "asc"',
		'[class.desc]': 'direction === "desc"',
		'(click)': 'rotate()'
	}
})
export class NgbdSortableHeader {
	@Input() sortable: SortColumn = '';
	@Input() direction: SortDirection = '';
	@Output() sort = new EventEmitter<SortEvent>();

	rotate() {
		this.direction = rotate[this.direction];
		this.sort.emit({ column: this.sortable, direction: this.direction });
	}
}
// ------

@Component({
	selector: 'app-results',
	templateUrl: './results.component.html',
	styleUrls: ['./results.component.scss'],
})

export class ResultsComponent implements OnInit , AfterViewChecked, OnDestroy {
	@Input()
	listOfVehicles: Array<any>;
	@Input()
	ownVehicle: OwnVehicleParams;
	@ViewChildren(NgbdSortableHeader) 
	headers: QueryList<NgbdSortableHeader>;

	resultsProps: any;
	lngProperties: any;
	mktProps: any;
	currencySymbol: string;
	initialListOfVehicles: Array<any>;
	vehicleDescription: any;
	priceHistory: any;
	priceHistoryTypeLabels: Array<any>;
	tooltipLoadingFlag: boolean;
	vhDescription$: Observable<any>;
	priceHistory$: Observable<any>;
	advertsSuscription: Subscription = new Subscription();
	currencySymbolBefore: boolean = false;

	constructor(
		private dataService: LiveRetailLibService,
				private VhDescriptionService: VehicleDescriptionService,
				private PriceHistoryService: PriceHistoryService) {
	}

	ngOnInit(){
		this.lngProperties = this.dataService.getLanguageProperties().config;
		this.mktProps = this.dataService.getMarketConfig().config;
		this.resultsProps = this.dataService.getLanguageProperties().results;
		if (this.mktProps.currencySymbol == '£'){
			this.currencySymbolBefore = true;
		}else{
			this.currencySymbolBefore = false;
		}
	}

	ngAfterViewChecked(){
		this.addPriceBar(this.listOfVehicles);
		this.initialListOfVehicles = this.listOfVehicles;
		// console.log(this.initialListOfVehicles)
	}

	getVhDescription(natCode){
		this.tooltipLoadingFlag = true;
		this.vhDescription$ = this.VhDescriptionService.resolveVhDescription(
			this.ownVehicle.ownVehicle.isoCountry, 
			this.ownVehicle.ownVehicle.isoLanguage, 
			natCode);
		this.vhDescription$.subscribe( a => {
			this.vehicleDescription = a;
			this.tooltipLoadingFlag = false;
			// console.log(this.vehicleDescription)
		})
	}

	getPriceHistory(id){
		this.tooltipLoadingFlag = true;
		this.priceHistory$ = this.PriceHistoryService.resolvePriceHistory(this.ownVehicle.ownVehicle.isoCountry, id);
		this.priceHistory$.subscribe( a => {
			this.priceHistory = a;
			this.tooltipLoadingFlag = false;
			// console.log(this.priceHistory)
		})
	}
	
	addPriceBar(vehicles){
		for (let i = 0; i <= vehicles?.length; i++){
			if(vehicles[i]) {
				let askPrice = vehicles[i].askingPrice;
				let regionalPrice = vehicles[i].regionalAskingPrice;
				let priceDifference = Math.abs(askPrice - regionalPrice);
				let totalPrice = askPrice + regionalPrice + priceDifference;
				let askPricePercentage = (100 * askPrice) / totalPrice;
				let regionalPricePercentage = (100 * regionalPrice) / totalPrice;
				document.getElementById('priceBarOne' + i).style.width = askPricePercentage + '%';
				document.getElementById('priceBarTwo' + i).style.width = regionalPricePercentage + '%';
			}
		}
	}

	onSort({ column, direction }: SortEvent) {
		// resetting other headers
		this.headers.forEach(header => {
			if (header.sortable !== column) {
				header.direction = '';
			}
		});
		// sorting columns
		if (direction === '' || column === '') {
			this.listOfVehicles = this.initialListOfVehicles;
		} else {
			this.listOfVehicles = [...this.listOfVehicles].sort((a, b) => {
				const res = compare(a[column], b[column]);
				return direction === 'asc' ? res : -res;
			});
		}
	}

	checkAll(ev) {
		this.listOfVehicles.forEach(x => x.state = ev.target.checked);
		console.log(ev)
	}

	isAllChecked() {
		return this.listOfVehicles.every(_ => _.state);
	}

	ngOnDestroy(): void {
		this.advertsSuscription.unsubscribe();
	}
}
