import { Component, ViewChild, Input, OnChanges, OnInit } from '@angular/core';
import { LiveRetailLibService } from '../../liveRetail-lib.service';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexGrid,
  ApexYAxis,
  ApexTooltip,
  ApexStroke,
  ApexDataLabels,
  ApexAnnotations,
  ApexNonAxisChartSeries
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  dataLabels: ApexDataLabels;
  labels: string[];
  colors: string[];
  tooltip: ApexTooltip;
  stroke: ApexStroke;
  grid: ApexGrid;
  annotations: ApexAnnotations;
};

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit, OnChanges {

  @Input()
  advertsObject: any;

  @ViewChild("chart") chart: ChartComponent;
  @Input() annotations: ApexAnnotations;
  @Input() colors: string[];
  @Input() dataLabels: ApexDataLabels;
  @Input() series: ApexAxisChartSeries | ApexNonAxisChartSeries;
  @Input() stroke: ApexStroke;
  @Input() labels: string[];
  @Input() tooltip: ApexTooltip;
  @Input() xaxis: ApexXAxis;
  @Input() yaxis: ApexYAxis | ApexYAxis[];
  @Input() grid: ApexGrid;

  public chartOptions: any;
  minVh: any;
  maxVh: any;
  ownVhPrice: any;
  avgAskingPrice: any;
  tooltipLoadingFlag: boolean;
  mktProps: any;
  reportProps: any;
  resultsProps: any;
  ownVehicleObj: any;

  constructor(private dataService: LiveRetailLibService) {
  }

  ngOnInit(){
    this.reportProps = this.dataService.getLanguageProperties().report;
    this.resultsProps = this.dataService.getLanguageProperties().results;
    this.mktProps = this.dataService.getMarketConfig().config;

  }

  ngOnChanges(){
    this.getGraphData();
  }

  getGraphData() {
    this.tooltipLoadingFlag = true;
    if(this.advertsObject){
      this.minVh = this.advertsObject.minValueVehicle;
      this.maxVh = this.advertsObject.maxValueVehicle;
      this.ownVhPrice = this.advertsObject.liveRetailPrice;
      this.avgAskingPrice = Math.round(this.advertsObject.averageAskingPrice);
      this.graphConfig();
      // console.log(this.minVh, this.maxVh)
      if(this.minVh.askingPrice == null && this.maxVh.askingPrice == null){
        this.tooltipLoadingFlag = true;
      }else {
        this.tooltipLoadingFlag = false;
      } 
    } 
    for (let i = 0; i < this.advertsObject?.vehicles.length; i++){
      if (this.advertsObject.vehicles[i].isOwnVehicle === true){
          this.ownVehicleObj = this.advertsObject.vehicles[i];
      }	
    }
    // console.log(this.ownVehicleObj)
  }

  graphConfig(){
    // graph parameters

    var minValue = this.minVh.askingPrice;
    var maxValue = this.maxVh.askingPrice;

    var avgValue = this.avgAskingPrice;
    var ownVehiclePrice = this.ownVhPrice;
    var currencySymbol = this.mktProps.currencySymbol;
    var thousandSymbol = this.mktProps.valueSeparator;

    var graphTextAvg = this.reportProps.graph.avg;
    var graphTextLiveRetailPrice = this.reportProps.graph.liveRetailPrice.toUpperCase();
    var graphTextLrp = this.reportProps.graph.lrp.toUpperCase();

    var avgWithCommas = avgValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandSymbol);
    var ownVehicleWithCommas = ownVehiclePrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandSymbol);
   
    var currencySymbolBefore = false;
    if(currencySymbol == '£') {
      currencySymbolBefore = true;
      var AVGLabelText = graphTextAvg + ' ' + currencySymbol + ' ' + avgWithCommas ;
      var LRPLabelText = graphTextLiveRetailPrice + ' ' + currencySymbol + ' ' + ownVehicleWithCommas;
    }else{
      var AVGLabelText = graphTextAvg + ' ' + avgWithCommas + ' ' + currencySymbol;

      var LRPLabelText = graphTextLiveRetailPrice + ' ' + ownVehicleWithCommas + ' ' + currencySymbol;
    }

    var lrp;
    if (ownVehiclePrice > maxValue) { lrp = maxValue } else { lrp = ownVehiclePrice };
    if (ownVehiclePrice < minValue) { lrp = minValue };

    

    this.chartOptions = {
      series: [{
        name: 'ASKING PRICE',
        data: [maxValue, lrp, minValue], //
      }],
      chart: {
        width: "100%",
        height: 200,
        type: 'line',
        zoom: {
          enabled: false
        },
      },
      dataLabels: {
        enabled: false
      },
      colors: ["#232C32"],
      stroke: {
        width: 3,
        curve: 'smooth',
        dashArray: 5
      },
      xaxis: {
        show: false,
        categories: [' ', graphTextLrp, ' '],
        axisTicks: {
          show: false,
        },
        axisBorder: {
          show: false,
        }
      },
      yaxis: {
        show: true,
        tickAmount: 1,
        min: minValue,
        max: maxValue,
        forceNiceScale: false,
        labels: {
          show: true,
          style: {
            color: '#A1A9AC',
            fontSize: '12px',
          },
          formatter: function numberWithCommas(x) {
            if (currencySymbolBefore) {
              return currencySymbol + ' ' + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandSymbol);
						}else {
              return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandSymbol) + ' ' + currencySymbol;
						}
          }
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: true,
          borderType: 'solid',
          color: '#B6C2C4',
          width: 5,
          offsetX: 0,
          offsetY: 0
        },
      },
      tooltip: {
        enabled: false,
      },
      grid: {
        show: true,
        borderColor: '#ECEFF0',
        strokeDashArray: 0,
        position: 'back',
        xaxis: {
          lines: {
            show: true
          }
        },
        yaxis: {
          lines: {
            show: true
          }
        },
      },

      annotations: {
        position: 'front',
        yaxis: [
          {
            y: avgValue,
            strokeDashArray: 5,
            opacity: 1,
            borderColor: "#008FC9",
            label: {
              borderColor: "#008FC9",
              position: 'left',
              offsetX: 70,
              offsetY: 7.5,
              style: {
                color: "#fff",
                background: "#008FC9",
              },
              text: AVGLabelText
            },
          },
        ],
        points: [
          {
            x: graphTextLrp,
            y: lrp,
            marker: {
              size: 6,
              fillColor: "#fff",
              strokeColor: "#232C32",
              radius: 2,
            },
            label: {
              borderColor: "#232C32",
              offsetY: 5,
              style: {
                color: "#fff",
                background: "#232C32"
              },
              text: LRPLabelText
            }
          }
        ]
      }
    }
  }

}
