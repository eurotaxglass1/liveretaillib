export interface OwnVehicleParams {
    ownVehicle: params|any;
}

interface params {
    isoCountry: String,
    isoLanguage: String,
    natCode: String|number,
    vehicleType: String,
    odometer: number|string,
    regDate: Date|string,
    regNo: String,
    regionCode: number|string,
    zipcode: String,
    askingPrice: number|string,
    optionValue?: number,
    optionList?: Array<any>|null
}