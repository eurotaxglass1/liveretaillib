export interface vehicleDescriptionTooltip{
    make: string;
    modelSeries: string;
    type: string;
    body: string;
    doors: string;
    seats: string;
    fuel: string;
    powerKW: number;
    powerHP: number;
    cylCubic: number;
    cylinders: number;
    gearBox: string;
    noOfGears: number;
    drive: string;
    engineType: string;
    wheelbase: string;
    totalWeight: number;
    payload: string;
    importBegin: Date;
    importEnd: Date;
    vehicleType: string;
    nationalCode: string
}