export interface Vehicle {
    vehicleId: number;
    imageLink: string;
    advertisements: Array<string>;
    officialTypeDescription: string;
    askingPrice: number;
    priceDifferenceCurrency: number;
    liveRetailPrice: number;
    similarityCategory: number;
    distance: number;
    odometer: number;
    regDate: Date;
    companyName: String;
    stockDays: number;
    numberOfPriceChanges: number;
}
