import { TestBed } from '@angular/core/testing';

import { LiveRetailLibService } from './liveRetail-lib.service';

describe('MyLibService', () => {
  let service: LiveRetailLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LiveRetailLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
