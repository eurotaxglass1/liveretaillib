import { NgModule } from '@angular/core';
import { LiveRetailLibComponent } from './liveRetail-lib.component';
import { ReportComponent } from './components/report/report.component';
import { SummaryComponent } from './components/summary/summary.component';
import { FiltersComponent } from './components/filters/filters.component';
import { NgbdSortableHeader, ResultsComponent } from './components/results/results.component';
import { MatchesComponent } from './components/matches/matches.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgApexchartsModule } from 'ng-apexcharts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {NumberLocalePipe} from './pipes/numberLocale.pipe';
import {PriceChangesMappingPipe} from './pipes/priceChanges-mapping.pipe';
import { RoundPipe } from './pipes/round.pipe';
import { ScatterGraphComponent } from './components/scatter-graph/scatter-graph.component';
import { ScrollUpButtonComponent } from './components/scroll-up-button/scroll-up-button.component';

@NgModule({
  declarations: [
    LiveRetailLibComponent,
    ReportComponent,
    SummaryComponent,
    FiltersComponent,
    ResultsComponent,
    MatchesComponent,
    NumberLocalePipe,
    PriceChangesMappingPipe,
    NgbdSortableHeader,
    ScatterGraphComponent,
    RoundPipe,
    ScrollUpButtonComponent
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgApexchartsModule,
    NgbTooltipModule,
    NgMultiSelectDropDownModule,
  ],
  exports: [
    LiveRetailLibComponent,
    ReportComponent,
    SummaryComponent,
    FiltersComponent,
    ResultsComponent,
    MatchesComponent,
    NumberLocalePipe,
    PriceChangesMappingPipe,
    NgbdSortableHeader,
    ScatterGraphComponent,
    RoundPipe,
    ScrollUpButtonComponent
  ]
})
export class LiveRetailLibModule { }
