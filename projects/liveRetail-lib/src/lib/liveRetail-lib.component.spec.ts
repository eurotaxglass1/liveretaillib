import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveRetailLibComponent } from './liveRetail-lib.component';

describe('MyLibComponent', () => {
  let component: LiveRetailLibComponent;
  let fixture: ComponentFixture<LiveRetailLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveRetailLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveRetailLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
