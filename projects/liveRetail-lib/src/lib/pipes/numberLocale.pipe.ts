import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberLocale'
})
export class NumberLocalePipe implements PipeTransform {

  transform(value: any, symbol: any): any {
    var userLang = navigator.language;
    if (value && symbol) return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, symbol);
    else return new Intl.NumberFormat(userLang).format(value);

  }

}