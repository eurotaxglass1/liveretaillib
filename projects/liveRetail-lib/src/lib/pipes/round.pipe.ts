import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'round'
})
export class RoundPipe implements PipeTransform {

  transform (input:number) {
    let x: string;
    if(input){
      x = parseFloat(input.toFixed(1)).toString().replace('.',',');
      return x;
    }
  }

}
