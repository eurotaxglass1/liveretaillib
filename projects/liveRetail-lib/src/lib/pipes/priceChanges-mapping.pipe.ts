import { Pipe, PipeTransform } from '@angular/core';
import { LiveRetailLibService } from '../liveRetail-lib.service';

@Pipe({
  name: 'priceChangesMapping'
})
export class PriceChangesMappingPipe implements PipeTransform {

  resultsProps: any;

  constructor(private dataService: LiveRetailLibService){
    this.resultsProps = this.dataService.getLanguageProperties().results;
  }

  transform(val: any): any {

    if (!val) {
      return;
    }

    // console.log(val);

    if (val == "n" || val == "original") {
        return this.resultsProps.tooltips.priceHistory.original;
    } 
    if (val == 'up' || val == "price_change" || val == "price") {
        return this.resultsProps.tooltips.priceHistory.priceCh;
    }
    if (val == "d" || val == "removed") {
        return this.resultsProps.tooltips.priceHistory.removed;
    } 
    if (val == "r" || val == "new" || val == "new_observation"){
        return this.resultsProps.tooltips.priceHistory.new;
    }

  }

}