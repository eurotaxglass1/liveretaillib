{
    "summary": {
        "lrPrice": "Valor spot eurotax",
        "avgStockDays": "Dias anunciado (media)",
        "desirability": "Conveniencia",
        "avgMileage": "Kilometraje (media)",
        "avgPriceChanges": "Cambios de precio (media)",
        "summaryTooltip": {
            "header": "Live Retail - real-time valuations",
            "content": "This function compares the asking price you have set to Glass’s LIVE Retail prices for similar vehicles for sale today in the market. Live Retail is a guide to indicate over and underpriced vehicles, taking into account characteristics such as exact age and mileage to individually value each vehicle.",
            "content2": "Note: Although vehicles can be very similar, they are never fully identical resulting in price differences.",
            "list": {
                "first": "Stock days - Average number of days the vehicle has been in stock.",
                "second": "Mileage - Average number of miles accumulated.",
                "third": "Desirability - The most desirable vehicle for generating margin and profit. Excellent - Good - Fair - Poor.",
                "fourth": "Price changes – The average number of retail price changes for this vehicle."
            }
        }
    },
    "report": {
        "graph": {
            "liveRetailPrice": "LIVE RETAIL PRICE",
            "lrp": "lrp",
            "avg": "AVG."
        },
        "heading": {
            "zip": "ZIP"
        },
        "scatterTooltip": {
            "header": "Visual retail price comparison",
            "content": "Moving the mouse cursor over the graph icon opens a visual price comparison. This gives an overview of advertised vehicles and their price difference from Glass’s Live Retail. Each coloured dot in the graph represents one vehicle and can be clicked to show the actual vehicle advert.",
            "list": {
                "first": "Green: Exact match",
                "second": "Blue: Very close match",
                "third": "Yellow: Close match",
                "fourth": "Black: This Vehicle"
            }
        }
    },
    "filters": {
        "mileage": "Kilometraje",
        "year": "Año",
        "keywords": "Palabras clave",
        "engineSize": "Motor",
        "fuel": "Combustible",
        "body": "Carrocería",
        "transmission": "Transmisión",
        "drive": "Tracción",
        "power": "Potencia",
        "distance": "Distancia",
        "seller": "Vendedor",
        "dealerSeller": "Distribuidor",
        "privateSeller": "Privado",
        "vehicles": "vehicles",
        "onSaleVehicles": "En venta",
        "soldVehicles": "Vendidos (60 days)",
        "showFilters": "Mostrar filtros",
        "hideFilters": "Ocultar filtros",
        "buttonSearch": "Buscar",
        "nullValid": "Tots",
        "wholeCountry": "Tot el país",
        "filtersTooltip": {
            "content": "The Live Retail search functionality allows users to filter the displayed vehicle results as follows.",
            "list": {
                "first": "Mileage",
                "second": "Registration date",
                "third": "Fuel type",
                "fourth": "Transmission",
                "fifth": "Engine Power",
                "sixth": "Keywords (eg Zetec, TDi Sport, S Line)",
                "seventh": "Engine displacement (CCM)",
                "eighth": "Body type",
                "ninth": "Drive type",
                "tenth": "Distance from dealership"
            }
        }
    },
    "matches": {
        "exact": "Exacte",
        "best": "Molt alta",
        "close": "Alta",
        "similar": "Mitjana",
        "matchesTooltip": {
            "content": "These icons illustrate how closely each vehicle shown in the list below matches your vehicle. The same colour icons are also used in the visual scatter graph comparison. The vehicles in the list can be ordered by clicking on each of the column headers."
        }
    },
    "results": {
        "ownVehicle": "El seu vehicle",
        "adLinks": "Links",
        "vehicle": "vehicle",
        "askPrice": "Preu d'anunci",
        "diffPrice": "VS",
        "lrPrice": "Valor spot Eurotax",
        "similarity": "Semblant",
        "distance": "Disténcia",
        "odometer": "Km",
        "regDate": "Data de Matrícula",
        "company": "Cod. Postal",
        "stockDays": "Díes anunciat",
        "priceChanges": "Hist\u00F2ric de preus",
        "tooltips": {
            "vhDescription": {
                "header": "Descripció completa",
                "make": "Marca",
                "modelSeries": "Model",
                "type": "Versió",
                "body": "Carrossería",
                "doors": "Portes",
                "seats": "Seients",
                "fuel": "Combustible",
                "powerKW": "Kw",
                "powerHP": "cv",
                "cylCubic": "cc",
                "cylinders": "Disposición",
                "gearBox": "Caixa de canvis",
                "noOfGears": "Nombre de marxes",
                "drive": "Tracció",
                "engineType": "Tipus de motor",
                "wheelBase": "Batalla",
                "totalWeight": "P.M.A",
                "payload": "Carga útil",
                "importBegin": "Inici comercializació",
                "importEnd": "Fi comercializació",
                "vhType": "Tipus de vehículo",
                "nationalCode": "Natcode"
            },
            "priceDiff": {
                "header": "Valor Spot Eurotax al detall",
                "standardVh": "Valor base según ofertas vigentes",
                "standardVhOdometer": "Quilometratge mitjé",
                "standardVhRegDate": "Edat mitjana",
                "thisVh": "Aquest vehicle",
                "odometerEffect": "Corrección de quilometratge",
                "ageEffect": "Correció d'edat",
                "regionalEffect": "Correcció regional",
                "options": "Equipament:",
                "equipmentAverage": "Avg options value - detailed options n/a",
                "totalEffects": "Total correccions",
                "regionalAskingPrice": "Valor Spot Eurotax",
                "regional": "Valor Spot Eurotax",
                "askingPrice": "Preu de publicación",
                "priceDifference": "Diferencia de preu",
                "underpriced": "Infravalorat",
                "overpriced": "Sobrepreu",
                "noOptions": "No afecta al preu"
            },
            "priceHistory": {
                "header": "Hist\u00F2ric de preus",
                "dateAdRemoved": "Último día de oferta",
                "removed": "Data de retirada del anunci",
                "new": "Nou anunci",
                "priceCh": "Canvi de preu",
                "original": "Anunci original"
            }
        }
    },
    "config": {
        "monthsTraslation": "Meses"
    },
    "errorCode": {
        "wrongAskingOrMileage": "Asking price and/or mileage are mandatory and cannot be 0 for live retail functionality to work. Please amend. If you experience further issues, then please contact our Customer Support team.",
        "onlyOwnVh": "There are no similar vehicles based on your chosen filtering options. Please change your filtering criteria, e.g. the radius of the search using the distance filter and try again."
    }
}