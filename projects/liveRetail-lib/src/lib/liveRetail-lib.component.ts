import { Component, Input, OnChanges, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { LiveRetailLibService } from './liveRetail-lib.service';
import { AdvertsService } from './services/adverts.service';
import { OwnVehicleParams } from './interfaces/own_vehicle';

@Component({
	selector: 'liveRetail-lib',
	templateUrl: './liveRetail-lib.component.html',
	styleUrls: [ './liveRetail-lib.component.scss' ],
	providers: [ NgbTooltipConfig, LiveRetailLibService ],
	encapsulation: ViewEncapsulation.None
})
export class LiveRetailLibComponent implements OnInit {
	listOfVehicles: any = null;
	initialListOfVehicles: any = null;
	advertsSuscription: any;
	adverts$: Observable<any>;
	advertsObject: any = null;
	noDataProvidedFlag: boolean = false;
	onlyOwnVhFlag: boolean = false;
	wrongDataProvidedFlag: boolean = false;
	noAskingOrMileageProvidedFlag: boolean = false;
	lngProps: any;
  

	@Input() ownVehicleParams: OwnVehicleParams;
	parentSuscription: any;

  constructor(
    config: NgbTooltipConfig, 
    private LiveRetailLibService: LiveRetailLibService,
	private advertsService: AdvertsService,
	private dataService: LiveRetailLibService,) {
		//TOOLTIP COMMON CONFIG.
		config.placement = 'bottom';
		config.triggers = 'click:blur';
    	config.autoClose = 'outside';
	}

	ngOnInit(): void {
		let ownVh = this.ownVehicleParams.ownVehicle;
		if(!ownVh){
			this.noDataProvidedFlag = true;
		}else {
			if (!ownVh.isoLanguage || !ownVh.isoCountry || !ownVh.natCode || 
				!ownVh.vehicleType || !ownVh.regDate || 
				!ownVh.zipcode) {
					this.wrongDataProvidedFlag = true;
				}
				if( !ownVh.askingPrice || ownVh.askingPrice < 1 || !ownVh.odometer || ownVh.odometer < 1){
					this.noAskingOrMileageProvidedFlag = true;
				}
		}	

    	this.LiveRetailLibService.getLanguageConfig(this.ownVehicleParams.ownVehicle.isoLanguage, this.ownVehicleParams.ownVehicle.isoCountry);
		this.adverts$ = this.advertsService.resolveAdverts(this.ownVehicleParams, null);
		this.getVehicles();
		this.getAdvertsObject();
		this.lngProps = this.dataService.getLanguageProperties();
	}

	getVehicles() {
		this.advertsSuscription = this.adverts$.subscribe((a) => {
			this.listOfVehicles = a.vehicles;
			if(this.listOfVehicles.length == 1){
				this.onlyOwnVhFlag = true;
			}
		});
	}

	getAdvertsObject() {
		this.adverts$.subscribe((a) => {
			this.advertsObject = a;
			// console.log(this.advertsObject)
		});
	}

	updateAdvertsFromFilters(data) {
		// console.log("Search Params from parent", data);
		this.listOfVehicles = null;
		this.adverts$ = this.advertsService.resolveAdverts(null, data);
		this.adverts$.subscribe((a) => {
			this.listOfVehicles = a.vehicles;
			this.advertsObject = a;
			// console.log("advertsObject from parent", this.advertsObject)
		});
	}

	closeModal(){
		this.onlyOwnVhFlag = false;
	}
}
