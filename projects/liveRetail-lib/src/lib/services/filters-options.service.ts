import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Headers }  from './../assets/headers';

@Injectable({
	providedIn: 'root'
})
export class FiltersOptionsService {
	// URL which returns list of JSON items (API end-point URL)
	private readonly URL = '/live-retail-api/search-criteria';
	constructor(private http: HttpClient) {}
	// every HTTTP call returns Observable object
	resolveFiltersOptions(isoCountry: String, isoLanguage: String, natCode: number|String): Observable<any> {
		// console.log(this.URL + '/' + isoCountry + '/' + isoLanguage + '/' + natCode, Headers)
		return this.http.get(
			this.URL + '/' + isoCountry + '/' + isoLanguage + '/' + natCode, Headers
		);
	}
}
