import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Headers }  from './../assets/headers'

@Injectable({
	providedIn: 'root'
})
export class PriceHistoryService {
	// URL which returns list of JSON items (API end-point URL)
	private readonly URL = '/live-retail-api/vehicle/priceHistory';
	constructor(private http: HttpClient) {}
	// every HTTTP call returns Observable object
	resolvePriceHistory(isoCountry: String, id: number|String): Observable<any> {
		// console.log('Request is sent!');
		return this.http.get(
			this.URL + '/' + isoCountry + '/' + id, Headers
		);
	}
}
