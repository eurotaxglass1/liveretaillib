import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Headers }  from './../assets/headers';
import { OwnVehicleParams } from '../interfaces/own_vehicle';

@Injectable({
	providedIn: 'root'
})
export class AdvertsService {

	searchParameters: Object;
	liveRetailParamaters: OwnVehicleParams;

	constructor(private http: HttpClient) {
		
	}
	// this method returns list-of-items in form of Observable
	// every HTTTP call returns Observable object
	resolveAdverts(ownVhParams?: OwnVehicleParams, searchParms?: Object): Observable<any> {
		if(ownVhParams){
			this.liveRetailParamaters = ownVhParams;
		}
		if (searchParms){
			this.searchParameters = searchParms;
		} else{
			this.searchParameters = {};
		}

		// URL which returns list of JSON items (API end-point URL)
		let URL = 
		'/live-retail-api/adverts/' + 
		this.liveRetailParamaters.ownVehicle.isoCountry + '/' + 
		this.liveRetailParamaters.ownVehicle.natCode + '/' +
		this.liveRetailParamaters.ownVehicle.vehicleType;
		// console.log(URL);

		let body = Object.assign(this.liveRetailParamaters, this.searchParameters);
		// console.log('Request is sent!', body);	
		return this.http.post(
			URL,
			body,
			Headers
		);
	}
}
