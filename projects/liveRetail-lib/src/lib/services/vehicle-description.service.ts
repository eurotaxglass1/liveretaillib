import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Headers }  from './../assets/headers';
@Injectable({
	providedIn: 'root'
})
export class VehicleDescriptionService {
	// URL which returns list of JSON items (API end-point URL)
	private readonly URL = '/live-retail-api/vehicle/description';
	constructor(private http: HttpClient) {}
	// this method returns list-of-items in form of Observable
	// every HTTTP call returns Observable object
	resolveVhDescription(isoCountry: String, isoLanguage: String, natCode: number|String): Observable<any> {
		// console.log('Request is sent!');
		// Using the GET method
		return this.http.get(
			this.URL + '/' + isoCountry + '/' + isoLanguage + '/' + natCode, Headers
		);
	}
}
