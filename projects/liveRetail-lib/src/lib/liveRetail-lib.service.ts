import { Injectable } from '@angular/core';
import es_ES from '../lib/assets/application-propierties/es-ES.json';
import en_ES from '../lib/assets/application-propierties/en-ES.json';
import en_UK from '../lib/assets/application-propierties/en-UK.json';
import ca_ES from '../lib/assets/application-propierties/ca-ES.json';
import de_UK from '../lib/assets/application-propierties/de-UK.json';
import de_AT from '../lib/assets/application-propierties/de-AT.json';
import de_CH from '../lib/assets/application-propierties/de-CH.json';
import de_DE from '../lib/assets/application-propierties/de-DE.json';
import fi_FI from '../lib/assets/application-propierties/fi-FI.json';
import fr_BE from '../lib/assets/application-propierties/fr-BE.json';
import fr_CH from '../lib/assets/application-propierties/fr-CH.json';
import fr_FR from '../lib/assets/application-propierties/fr-FR.json';
import it_CH from '../lib/assets/application-propierties/it-CH.json';
import it_IT from '../lib/assets/application-propierties/it-IT.json';
import nl_BE from '../lib/assets/application-propierties/nl-BE.json';
import pl_PL from '../lib/assets/application-propierties/pl-PL.json';
import pt_PT from '../lib/assets/application-propierties/pt-PT.json';
import se_SE from '../lib/assets/application-propierties/se-SE.json';
import default_localize from '../lib/assets/application-propierties/default.json';

import UK_mkt from '../lib/assets/application-propierties/market-config/market-UK.json';
import AT_mkt from '../lib/assets/application-propierties/market-config/market-AT.json';
import ES_mkt from '../lib/assets/application-propierties/market-config/market-ES.json';
import DE_mkt from '../lib/assets/application-propierties/market-config/market-DE.json';
import CH_mkt from '../lib/assets/application-propierties/market-config/market-CH.json';
import BE_mkt from '../lib/assets/application-propierties/market-config/market-BE.json';
import NL_mkt from '../lib/assets/application-propierties/market-config/market-NL.json';
import PL_mkt from '../lib/assets/application-propierties/market-config/market-PL.json';
import PT_mkt from '../lib/assets/application-propierties/market-config/market-PT.json';
import IT_mkt from '../lib/assets/application-propierties/market-config/market-IT.json';
import FR_mkt from '../lib/assets/application-propierties/market-config/market-FR.json';
import FI_mkt from '../lib/assets/application-propierties/market-config/market-FI.json';
import SE_mkt from '../lib/assets/application-propierties/market-config/market-SE.json';


@Injectable({
  providedIn: 'root'
})
export class LiveRetailLibService {
	lngConfig: String;

    constructor() {
    }

	getLanguageConfig(lng?: String, market?: String) {
		if (lng && market) this.lngConfig = lng + "-" + market;
		// console.log(this.lngConfig)
		// this.lngConfig = ''; // SET UP HERE LANGUAGE DEMO FOR APP.PROPERTIES
		return this.lngConfig;
	}

	getCountry(){
		let market = this.lngConfig.slice(-2);
		return market;
	}

	getMarketConfig(){
		let market = this.lngConfig.slice(-2);
		// console.log(market)
		switch (market) {
			case 'UK':
			case 'GB': //support GB IsoCountry
				return UK_mkt;
			case 'AT':
				return AT_mkt;
			case 'ES':
				return ES_mkt;
			case 'DE':
				return DE_mkt;
			case 'CH':
				return CH_mkt;
			case 'BE':
				return BE_mkt;
			case 'FI':
				return FI_mkt;
			case 'FR':
				return FR_mkt;
			case 'IT':
				return IT_mkt;
			case 'NL':
				return NL_mkt;
			case 'PL':
				return PL_mkt;
			case 'PT':
				return PT_mkt;
			case 'SE':
				return SE_mkt;
    	}
	}

	getLang(){
		let lng = this.lngConfig.slice(0,2)
		// console.log(lng)
		return lng;
	}

	getLanguageProperties() {
		switch (this.getLanguageConfig()) {
			case 'en-UK':
			case 'en-GB': //support GB IsoCountry
				return en_UK;
			case 'en-ES':
				return en_ES;
			case 'es-ES':
				return es_ES;
			case 'ca-ES':
				return ca_ES;
			case 'de-UK':
			case 'de-GB':
				return de_UK;
			case 'de-AT':
				return de_AT;
			case 'de-CH':
				return de_CH;
			case 'de-DE':
				return de_DE;
			case 'fi-FI':
				return fi_FI;
			case 'fr-BE':
				return fr_BE;
			case 'fr-CH':
				return fr_CH;
			case 'fr-FR':
				return fr_FR;
			case 'it-CH':
				return it_CH;
			case 'it-IT':
				return it_IT;
			case 'nl-BE':
				return nl_BE;
			case 'pl-PL':
				return pl_PL;
			case 'pt-PT':
				return pt_PT;
			case 'se-SE':
        		return se_SE;
			default:
				return default_localize;
    }
  }
}
